<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Education_LMS
 */

$show_footer_connect    = esc_attr( get_theme_mod( 'hide_footer_social', '1' ) );
$footer_layout    = esc_attr( get_theme_mod( 'footer_width', 'contained' ) );
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">

		<?php if ( $show_footer_connect != 1 ) { ?>
        <div class="footer-connect">
            <div class="container">
                <div class="footer-social">
                    <label><?php echo esc_html( get_theme_mod('follow_title',  esc_html__('Follow Us', 'education-lms') ) )  ?></label>
                    <?php education_lms_social_media(); ?>
                </div>
            </div>
        </div>
		<?php } ?>

		<div id="footer" class="<?php echo ( $footer_layout == 'full' ) ? 'container-fluid' : 'container' ?>">

			<?php
           
			$footer_columns = absint( get_theme_mod( 'footer_layout' , 4 ) );
			$max_cols = 12;
			$layouts = 12;
			if ( $footer_columns > 1 ){
				$default = "12";
				switch ( $footer_columns ) {
					case 4:
						$default = '3+3+3+3';
						break;
					case 3:
						$default = '4+4+4';
						break;
					case 2:
						$default = '6+6';
						break;
				}
				$layouts = esc_html( get_theme_mod( 'footer_custom_'.$footer_columns.'_columns', $default ) );
			}
			$layouts = explode( '+', $layouts );
			foreach ( $layouts as $k => $v ) {
				$v = absint( trim( $v ) );
				$v =  $v >= $max_cols ? $max_cols : $v;
				$layouts[ $k ] = $v;
			}
			$have_widgets = false;
			for ( $count = 0; $count < $footer_columns; $count++ ) {
				$id = 'footer-' . ( $count + 1 );
				if ( is_active_sidebar( $id ) ) {
					$have_widgets = true;
				}
			}
			if ( $footer_columns > 0 && $have_widgets ) { ?>
                <div class="footer-widgets">
                    <div class="row">
						<?php
						for ( $count = 0; $count < $footer_columns; $count++ ) {
							$col = isset( $layouts[ $count ] ) ? $layouts[ $count ] : '';
							$id = 'footer-' . ( $count + 1 );
							if ( $col ) {
								?>
                                <div id="footer-<?php echo esc_attr( $count + 1 ) ?>" class="col-md-<?php echo esc_attr( $col ); ?> " >
									<?php dynamic_sidebar( $id ); ?>
                                </div>
								<?php
							}
						}
						?>
                    </div>
                </div>
			<?php }   ?>

            <?php ?>

            <div class="copyright-area">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="site-info">
	                        <?php do_action( 'education_lms_footer_copyright' ); ?>
                        </div><!-- .site-info -->
                    </div>
                    <div class="col-sm-6">
                        <?php
                        if ( has_nav_menu( 'menu-2' ) ) {
                            wp_nav_menu( array(
                                'theme_location' => 'menu-2',
                                'menu_id'        => 'footer-menu',
                                'menu_class'     => 'pull-right list-unstyled list-inline mb-0'
                            ) );
                        }
                        ?>

                    </div>
                </div>
            </div>

        </div>
	</footer><!-- #colophon -->

    <?php ?>
    <a href="#top" id="to-top" title="Back to top"><i class="fa fa-angle-double-up"></i></a>
    <?php  ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
