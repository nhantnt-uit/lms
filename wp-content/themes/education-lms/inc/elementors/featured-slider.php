<?php
namespace EducationLMS\Widgets;
use  Elementor\Widget_Base ;
use  Elementor\Controls_Manager ;
use  Elementor\Repeater;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Education_LMS_Featured_Slider extends Widget_Base {
	public function get_name() {
		return 'edu-featured-slider';
	}
	public function get_title() {
		return __( 'Featured Slider', 'education-lms' );
	}
	public function get_icon() {
		// Icon name from the Elementor font file, as per http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-slideshow';
	}
	public function get_categories() {
		return [ 'edu-elements' ];
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_content',
			[
				'label' => esc_html__( 'Slider', 'education-lms' ),
			]
		);

		$repeater = new Repeater();
		$repeater->start_controls_tabs( 'slides_repeater' );

		$repeater->start_controls_tab( 'background', [ 'label' => __( 'Background', 'education-lms' ) ] );

		$repeater->add_control(
			'background_color',
			[
				'label' => __( 'Color', 'education-lms' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#bbbbbb',
				'selectors' => [
					'{{WRAPPER}} {{CURRENT_ITEM}} .feature-slide-bg' => 'background-color: {{VALUE}}',
				],
			]
		);

		$repeater->add_control(
			'background_image',
			[
				'label' => _x( 'Image', 'Background Control', 'education-lms' ),
				'type' => Controls_Manager::MEDIA,
				'selectors' => [
					'{{WRAPPER}} {{CURRENT_ITEM}} .feature-slide-bg' => 'background-image: url({{URL}})',
				],
			]
		);
		$repeater->end_controls_tab();

		$repeater->start_controls_tab( 'content', [ 'label' => __( 'Content', 'education-lms' ) ] );

		$repeater->add_control(
			'heading',
			[
				'label' => __( 'Title', 'education-lms' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Slide Heading', 'education-lms' ),
				'label_block' => true,
			]
		);
	
			$repeater->add_control(
				'title_color',
				[
					'label' => __( 'Title Color', 'education-lms' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .feature-slide-heading' => 'color: {{VALUE}} ;',
					],
				]
			);
			$repeater->add_responsive_control(
				'title_font_size',
				[
					'label' => __( 'Font Size', 'education-lms' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 10,
							'max' => 100,
						],
					],
					'default' => [
						'size' => 45,
					],
					'size_units' => [ 'px' ],
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .feature-slide-heading' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$repeater->add_control(
				'title_font_family',
				[
					'label' => __( 'Title Typography', 'education-lms' ),
					'type' => Controls_Manager::FONT,
					'default' => "'Montserrat', sans-serif",
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .feature-slide-heading' =>  'font-family: {{VALUE}}',
					],
				]
            );
        

		$repeater->add_control(
			'description',
			[
				'label' => __( 'Description', 'education-lms' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => __( 'I am slide content. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'education-lms' ),
				'show_label' => true,
			]
		);
	
			$repeater->add_control(
				'description_color',
				[
					'label' => __( 'Description Color', 'education-lms' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .feature-slide-description' => 'color: {{VALUE}};',
					],
				]
			);
			$repeater->add_responsive_control(
				'desc_font_size',
				[
					'label' => __( 'Font Size', 'education-lms' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 10,
							'max' => 100,
						],
					],
					'default' => [
						'size' => 18,
					],
					'size_units' => [ 'px' ],
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .feature-slide-description' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);
			$repeater->add_control(
				'description_font_family',
				[
					'label' => __( 'Description Typography', 'education-lms' ),
					'type' => Controls_Manager::FONT,
					'default' => "'Montserrat', sans-serif",
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .feature-slide-description' =>  'font-family: {{VALUE}}',
					],
				]
			);
		

		$repeater->add_control(
			'button_text',
			[
				'label' => __( 'Button Text', 'education-lms' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Click Here', 'education-lms' ),
			]
		);

		$repeater->add_control(
			'link',
			[
				'label' => __( 'Link', 'education-lms' ),
				'type' => Controls_Manager::URL,
				'placeholder' => __( 'http://your-link.com', 'education-lms' ),
			]
		);

	

			$repeater->add_control(
				'button_color',
				[
					'label' => __( 'Button Color', 'education-lms' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .slide-button' => 'background-color: {{VALUE}};',
						'{{WRAPPER}} {{CURRENT_ITEM}} .slide-button:hover' => 'border-color: {{VALUE}};',
					],
				]
			);

			$repeater->add_responsive_control(
				'align',
				[
					'label'     => __( 'Alignment', 'education-lms' ),
					'type'      => Controls_Manager::CHOOSE,
					'options'   => [
						'left'   => [
							'title' => __( 'Left', 'education-lms' ),
							'icon'  => 'fa fa-align-left',
						],
						'center' => [
							'title' => __( 'Center', 'education-lms' ),
							'icon'  => 'fa fa-align-center',
						],
						'right'  => [
							'title' => __( 'Right', 'education-lms' ),
							'icon'  => 'fa fa-align-right',
						],

					],
					'default'   => '',
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .feature-slide-inner' => 'text-align: {{VALUE}};',
					],
				]
			);
		
		$repeater->end_controls_tab();
		
		$repeater->end_controls_tabs();

		$this->add_control(
			'slides',
			[
				'label' => __( 'Slides', 'education-lms' ),
				'type' => Controls_Manager::REPEATER,
				'show_label' => true,
				'default' => [
					[
						'heading' => __( 'Slide 1 Heading', 'education-lms' ),
						'description' => __( 'Click edit button to change this text. Lorem ipsum dolor sit amet consectetur adipiscing elit dolor', 'education-lms' ),
						'button_text' => __( 'Click Here', 'education-lms' ),
						'background_color' => '#833ca3',
					],
					[
						'heading' => __( 'Slide 2 Heading', 'education-lms' ),
						'description' => __( 'Click edit button to change this text. Lorem ipsum dolor sit amet consectetur adipiscing elit dolor', 'education-lms' ),
						'button_text' => __( 'Click Here', 'education-lms' ),
						'background_color' => '#4054b2',
					],
				],
				'fields' => array_values( $repeater->get_controls() ),
				'title_field' => '{{{ heading }}}',
			]
		);

		$this->add_responsive_control(
			'slides_height',
			[
				'label' => __( 'Height', 'education-lms' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 100,
						'max' => 1000,
					],
					'vh' => [
						'min' => 10,
						'max' => 100,
					],
				],
				'default' => [
					'size' => 400,
				],
				'size_units' => [ 'px', 'vh', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .slick-slide' => 'height: {{SIZE}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

        $this->end_controls_section();
        
       
       
        $this->start_controls_section(
			'section_settings',
			[
				'label' => esc_html__( 'Settings', 'education-lms' ),
			]
        );
        
        $this->add_control(
			'autoplay',
			[
				'label' => __( 'Autoplay', 'education-lms' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'education-lms' ),
				'label_off' => __( 'No', 'education-lms' ),
				'return_value' => 'yes',
				'default' => 'no',
			]
        );
        
        $this->add_control(
			'arrows',
			[
				'label' => __( 'Show Arrows', 'education-lms' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'education-lms' ),
				'label_off' => __( 'No', 'education-lms' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
        );
        
        $this->add_control(
			'dots',
			[
				'label' => __( 'Show dot indicators', 'education-lms' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'education-lms' ),
				'label_off' => __( 'No', 'education-lms' ),
				'return_value' => 'yes',
				'default' => 'no',
			]
        );
        
        $this->add_control(
			'pauseOnHover',
			[
				'label' => __( 'Pause Autoplay On Hover', 'education-lms' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'education-lms' ),
				'label_off' => __( 'No', 'education-lms' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
        );


        $this->add_control(
			'autoplaySpeed',
			[
				'label' => __( 'Autoplay Speed', 'education-lms' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1000,
				'max' => 10000,
				'step' => 1000,
				'default' => 3000,
			]
		);
        
        $this->add_control(
			'speed',
			[
				'label' => __( 'Animation Speed', 'education-lms' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 100,
				'max' => 5000,
				'step' => 100,
				'default' => 300,
			]
        );
    
        $this->add_control(
			'arrows_color',
			[
				'label' => __( 'Arrows Color', 'education-lms' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .slick-arrow' => 'background-color: {{VALUE}}',
				],
			]
        );
        
        $this->add_control(
			'dots_color',
			[
				'label' => __( 'Dots Color', 'education-lms' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .slick-dots button' => 'background-color: {{VALUE}}',
				],
			]
        );
        
        $this->add_control(
			'dots_active_color',
			[
				'label' => __( 'Dot Active Color', 'education-lms' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .slick-dots li.slick-active button' => 'background-color: {{VALUE}}',
				],
			]
		);
        
        $this->end_controls_section();
        
	}


	protected function render( $instance = [] ) {
		$settings = $this->get_settings();

		if ( empty( $settings['slides'] ) ) {
			return;
		}

		$this->add_render_attribute( 'button', 'class', [ 'btn', 'slide-button' ] );



		$slides = [];
		$slide_count = 0;
		foreach ( $settings['slides'] as $slide ) {
			$slide_html = $slide_attributes = $btn_attributes = '';
			$btn_element = $slide_element = 'div';
			$slide_url = $slide['link']['url'];

			$slide_html .= '<div class="feature-slide-content animated fadeInUp">';

			if ( $slide['heading'] ) {
				$slide_html .= '<h2 class="feature-slide-heading">' . $slide['heading'] . '</h2>';
			}

			if ( $slide['description'] ) {
				$slide_html .= '<p class="feature-slide-description">' . $slide['description'] . '</p>';
			}

			if ( $slide['button_text'] && ! empty( $slide_url ) ) {
				$this->add_render_attribute( 'slide_link' . $slide_count , 'href', $slide_url );

				if ( $slide['link']['is_external'] ) {
					$this->add_render_attribute( 'slide_link' . $slide_count, 'target', '_blank' );
				}

				$btn_element = 'a';
				$btn_attributes = $this->get_render_attribute_string( 'slide_link' . $slide_count );
				$slide_html .= '<' . $btn_element . ' ' . $btn_attributes . ' ' . $this->get_render_attribute_string( 'button' ) . '>' . $slide['button_text'] . '</' . $btn_element . '>';
			}

			$slide_html .= '</div>';

			$slide_html = '<div class="feature-slide-bg"></div><' . $slide_element . ' ' . $slide_attributes . ' class="feature-slide-inner">' . $slide_html . '</' . $slide_element . '>';
			$slides[] = '<div class="elementor-repeater-item-' . $slide['_id'] . ' slick-slide">' . $slide_html . '</div>';

			$slide_count++;
		}
		$slider_class = 'slider-'.uniqid();
		$carousel_classes = [ 'feature-slider' ];
		$carousel_classes[] = $slider_class;
		$this->add_render_attribute( 'slides', [
			'class' => $carousel_classes,
			'data-animation' => 'up',
		] );

		?>

        <div class="feature-slides-wrapper feature-slick-slider" >
            <div <?php echo $this->get_render_attribute_string( 'slides' ); ?>>
				<?php echo implode( '', $slides ); ?>
            </div>
        </div>
        <script type="text/javascript">
            (function($) {
                "use strict";
                $('.<?php echo esc_attr($slider_class) ?>').slick({    
                    <?php ?>                
                    autoplay: <?php echo ( $settings['autoplay'] == 'yes' ) ? 'true' : 'false'; ?>,
                    arrows: <?php echo ( $settings['arrows'] == 'yes' ) ? 'true' : 'false'; ?>,
                    dots: <?php echo ( $settings['dots'] == 'yes' ) ? 'true' : 'false'; ?>,                    
                    pauseOnHover: <?php echo ( $settings['pauseOnHover'] == 'yes' ) ? 'true' : 'false'; ?>,
                    autoplaySpeed: <?php echo $settings['autoplaySpeed']; ?>,
                    speed: <?php echo $settings['speed']; ?>,     
                    <?php  ?>      
                    <?php if( is_rtl() ) { echo "rtl: true"; } ?>
                });
            })(jQuery);
        </script>
		<?php
	}

	protected function _content_template() {}

}