<?php

namespace EducationLMS\Widgets;

use  Elementor\Widget_Base;
use  Elementor\Controls_Manager;


if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly


class Education_LMS_Course_Search extends Widget_Base {
	public function get_name() {
		return 'edu-course-search';
	}

	public function get_title() {
		return __( 'Course Search', 'education-lms' );
	}

	public function get_icon() {
		return 'fa fa-search';
	}

	public function get_categories() {
		return [ 'edu-elements' ];
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_content',
			[
				'label' => esc_html__( 'Courses', 'education-lms' ),
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => __( 'Title', 'education-lms' ),
				'label_block' => true,
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'Search Courses', 'education-lms' )
			]
        );
        
        $this->add_control(
			'description',
			[
				'label'       => __( 'Description Before Input', 'education-lms' ),
				'label_block' => true,
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'Own your future learning new skills online', 'education-lms' )
			]
        );
        
        $this->add_control(
			'placeholder',
			[
				'label'       => __( 'Input Placeholder', 'education-lms' ),
				'label_block' => true,
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'What do you want to learn today?', 'education-lms' )
			]
		);


        $this->add_control(
			'long_description',
			[
				'label'       => __( 'Description After Input', 'education-lms' ),
				'label_block' => true,
				'type'        => Controls_Manager::WYSIWYG,
				'default'     => ''
			]
        );


		$this->end_controls_section();
	}


	protected function render( $instance = [] ) {
		$settings = $this->get_settings();

        ?>
        <div class="courses-search-widget">
           <div class="search-widget-inner">
            <?php if( $settings['title'] ) { ?><h3><?php echo esc_attr( $settings['title'] ); ?></h3> <?php } ?>
                <?php if( $settings['description'] ) { ?><p><?php echo esc_attr( $settings['description'] ); ?></p><?php } ?>
                <form method="get" name="search-course" class="learn-press-search-course-form"
                    action="<?php echo learn_press_get_page_link( 'courses' ); ?>">

                    <input type="text" name="s" class="search-course-input" value=""
                        placeholder="<?php echo esc_attr( $settings['placeholder'] ); ?>" autocomplete="off"/>
                    <input type="hidden" name="ref" value="course"/>

                    <button type="submit" class="search-submit" >
                        <i class="fa fa-search"></i>
                    </button>
                </form>
                <div class="long_desc"><?php echo  $settings['long_description']; ?></div>
           </div>
        </div>
        
        <?php
        
		wp_reset_postdata();
	}


}