<?php
function education_lms_course_widget() {
	register_widget( 'Education_LMS_Course_Widget' );
}
add_action( 'widgets_init', 'education_lms_course_widget' );

class Education_LMS_Course_Widget extends WP_Widget {
	function __construct() {
		parent::__construct(
			'education-lms-courses',
			esc_html__( 'Theme: Courses', 'education-lms' ),
			array(
				'classname' => 'theme-posts-widget',
				'description' => esc_html__( 'Display the popular or featured courses.', 'education-lms' ),
				'customize_selective_refresh' => true
			)
		);
	}
	function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, array(
			'number' => 4,
			'orderby' => 'date',
			'order' => 'desc',
			'category' => '',
			'featured'   => 'featured'
		) );

		global $post;

		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$post_args = array(
			'posts_per_page' => absint( $instance['number'] ),
			'order' =>   $instance['order'],
			'orderby' => $instance['orderby'],
			'post_type' => 'lp_course'
		);

		if ( $instance['category'] > 0 ) {
			$post_args['tax_query'] = array(
				array(
					'taxonomy' => 'course_category',
					'field'    => 'term_id',
					'terms'    => $instance['category']
				),
			);
		}
		if( $instance['featured'] == 1 ) {
			$post_args['meta_query'] = array(
				array(
					'key' => '_lp_featured',
					'value'    => 'yes',
					'compare'    => '='
				),
			);
		}

		if ( $post->ID > 0 ) {
			$post_args['post__not_in'] = array($post->ID);
		}

		$courses = new WP_Query( $post_args );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		if ( $courses->have_posts() ) {

			?>
			<ul class="widget-courses list-unstyled"><?php
			while ( $courses->have_posts() ) {
				$courses->the_post();

				?>
				<li>
					<?php if ( has_post_thumbnail() ) { ?>
						<div class="p-thumbnail">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
						</div>
					<?php } ?>
					<div class="p-info">
						<h3 class="entry-title"><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>" rel="bookmark"><strong><?php the_title(); ?></strong></a></h3>
						<?php  education_lms_course_price(); ?>
                        <?php education_lms_course_ratings() ?>
					</div>
				</li>
				<?php
			}
			?></ul><?php
		}
		wp_reset_postdata();
		echo $args['after_widget'];
	}
	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array(
			'title' => esc_html__( 'Recent Courses', 'education-lms' ),
			'number' => 4,
			'orderby' => 'date',
			'order' => 'desc',
			'category' => '',
			'featured' => ''
		) );
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Recent Courses', 'education-lms' );
		$number = absint( $instance['number'] );
		$orderby = $instance['orderby'];
		$order = $instance['order'];
		$cat = $instance['category'];
		$featured = $instance['featured'];
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'education-lms' ); ?>:
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'featured' ) ); ?>">
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'featured' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'featured' ) ); ?>" type="checkbox" <?php checked($featured, 1 , true) ?> value="1" /> Featured courses only
			</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of photos', 'education-lms' ); ?>:
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
			</label>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>"><?php esc_html_e( 'Order by', 'education-lms' ); ?>:</label>
			<select id="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'orderby' ) ); ?>" class="widefat">
				<option value="date" <?php selected( 'date', $orderby ) ?>><?php esc_html_e( 'Date', 'education-lms' ); ?></option>
				<option value="title" <?php selected( 'title', $orderby ) ?>><?php esc_html_e( 'Title', 'education-lms' ); ?></option>
				<option value="comment_count" <?php selected( 'Comment count', $orderby ) ?>><?php esc_html_e( 'Comment count', 'education-lms' ); ?></option>
				<option value="rand" <?php selected( 'rand', $orderby ) ?>><?php esc_html_e( 'Random', 'education-lms' ); ?></option>
			</select>
		</p>
		<p><label for="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>"><?php esc_html_e( 'Order', 'education-lms' ); ?>:</label>
			<select id="<?php echo esc_attr( $this->get_field_id( 'order' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'order' ) ); ?>" class="widefat">
				<option value="DESC" <?php selected( 'DESC', $order ) ?>><?php esc_html_e( 'DESC', 'education-lms' ); ?></option>
				<option value="ASC" <?php selected( 'ASC', $order ) ?>><?php esc_html_e( 'ASC', 'education-lms' ); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>">
				<?php esc_html_e( 'Category', 'education-lms' ); ?>:
			</label>
			<?php
			$course_cat = get_terms( array( 'taxonomy'=> 'course_category', 'hide_empty' => false ) );
			if ( ! empty( $course_cat ) && ! is_wp_error( $course_cat ) ){
				echo '<select name="'. $this->get_field_name( 'category' ) .'">';
				echo '<option value="">All</option>';
				foreach ( $course_cat as $category ) {
					echo '<option '. selected( $cat, $category->term_id ) .' value="'.$category->term_id.'">' . $category->name . '</option>';
				}
				echo '</select>';
			}

			?>
		</p>
		<?php
	}
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$new_instance = wp_parse_args( (array) $new_instance, array(
			'title' => '',
			'number' => 4,
			'orderby' => 'date',
			'order' => 'desc',
			'category' => '',
			'featured' => ''
		) );
		$instance['title'] =  sanitize_text_field( $new_instance['title'] );
		$instance['number'] = ! absint( $new_instance['number'] ) ? 4 : absint( $new_instance['number'] );
		$instance['orderby'] =  sanitize_text_field( $new_instance['orderby'] );
		$instance['order'] =  sanitize_text_field( $new_instance['order'] );
		$instance['category'] = absint( $new_instance['category'] );
		$instance['featured'] = absint( $new_instance['featured'] );

		return $instance;
	}
}