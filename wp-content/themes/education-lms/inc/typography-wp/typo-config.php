<?php
// Call this plugin in theme
if ( ! defined( 'TYPOGRAPHY_WP_URL' ) ) {
	define('TYPOGRAPHY_WP_URL', trailingslashit( get_template_directory_uri() . '/inc/typography-wp' ) );
}
if ( ! defined( 'TYPOGRAPHY_WP_PATH' ) ) {
	define('TYPOGRAPHY_WP_PATH', trailingslashit( get_template_directory() . '/inc/typography-wp') );
}
require get_template_directory() . '/inc/typography-wp/typography-wp.php';


// Call inside After theme setup function

/**
 * Just a example see how to use/config this plugin
 */
add_theme_support( 'typography_wp',
    array(
        'id' => 'body_typo',
        'label' => 'Body Typography',
        'selector'       => 'body',
        'priority'      => 100,
        'fields' => array(
            'family'         => '', // remove key if don't want to use
            'category'       => '',
            'fontId'         => '',
            'fontType'       => '',
            'subsets'        => '',
            'variant'        => '',
            'textColor'      => '', // remove key if don't want to use
            'fontWeight'     => '', // remove key if don't want to use
            'fontSize'       => '', // remove key if don't want to use
            'lineHeight'     => '', // remove key if don't want to use
        )
    ),

	array(
		'id' => 'site_title_typo',
		'label' => 'Site Title Typography',
		'selector'       => '.site-title',
		'priority'      => 15,
		'fields' => array(
			'family'         => '', // remove key if don't want to use
			'fontStyle'      => '',
			'fontWeight'     => '',
			'fontSize'       => '',
			'lineHeight'     => '',
			'letterSpacing'  => '',
			'textTransform'  => '',
			'textDecoration' => '',
		)
	),

	array(
		'id' => 'site_tagline_typo',
		'label' => 'Site Tagline Typography',
		'selector'       => '.site-description',
		'priority'      => 15,
		'fields' => array(
			'family'         => '', // remove key if don't want to use
			'fontStyle'      => '',
			'fontWeight'     => '',
			'fontSize'       => '',
			'lineHeight'     => '',
			'letterSpacing'  => '',
			'textTransform'  => '',
			'textDecoration' => '',
		)
	),

	array(
		'id' => 'primary_menu_typo',
		'label' => 'Primary Menu Typography',
		'selector'       => '.main-navigation a, .nav-menu ul li a',
		'priority'      => 15,
		'fields' => array(
			'family'         => '', // remove key if don't want to use
			'fontStyle'      => '',
			'fontWeight'     => '',
			'fontSize'       => '',
			'lineHeight'     => '',
			'letterSpacing'  => '',
			'textTransform'  => '',
			'textDecoration' => '',
		)
	),

    array(
        'id' => 'heading_typo',
        'label' => 'Heading Typography',
        'selector'       => 'h1,h2,h3,h4,h5,h6',
        'priority'      => 15,
        'fields' => array(
            'family'         => '',
            'category'       => '',
            'fontId'         => '',
            'fontType'       => '',
            'subsets'        => '',
            'variant'        => '',
            'textColor'      => '',
            'fontStyle'      => '',
            'fontWeight'     => '',
            'fontSize'       => '',
            'lineHeight'     => '',
            'letterSpacing'  => '',
            'textTransform'  => '',
            'textDecoration' => '',
        )
    ),

    array(
        'id' => 'titlebar_typo',
        'label' => 'Titlebar Typography',
        'selector'       => '.titlebar .header-title',
        'priority'      => 15,
        'fields' => array(
            'family'         => '', // remove key if don't want to use
            'fontStyle'      => '',
            'fontWeight'     => '',
            'fontSize'       => '',
            'lineHeight'     => '',
            'letterSpacing'  => '',
            'textTransform'  => '',
            'textDecoration' => '',
        )
    ),

	array(
		'id' => 'sidebar_widget_typo',
		'label' => 'Sidebar Widget Typography',
		'selector'       => '.widget-area .widget-title',
		'priority'      => 15,
		'fields' => array(
			'family'         => '', // remove key if don't want to use
			'fontStyle'      => '',
			'fontWeight'     => '',
			'fontSize'       => '',
			'lineHeight'     => '',
			'letterSpacing'  => '',
			'textTransform'  => '',
			'textDecoration' => '',
		)
	),

	array(
		'id' => 'footer_typo',
		'label' => 'Footer Widget Typography',
		'selector'       => '.site-footer .widget-title',
		'priority'      => 15,
		'fields' => array(
			'family'         => '', // remove key if don't want to use
			'fontStyle'      => '',
			'fontWeight'     => '',
			'fontSize'       => '',
			'lineHeight'     => '',
			'letterSpacing'  => '',
			'textTransform'  => '',
			'textDecoration' => '',
		)
	),

	array(
		'id' => 'breadcrumb_typo',
		'label' => 'Breadcrumb Typography',
		'selector'       => '.breadcrumbs',
		'priority'      => 15,
		'fields' => array(
			'family'         => '', // remove key if don't want to use
			'fontStyle'      => '',
			'fontWeight'     => '',
			'fontSize'       => '',
			'lineHeight'     => '',
			'letterSpacing'  => '',
			'textTransform'  => '',
			'textDecoration' => '',
		)
	)

);