<?php
/**
 * Calls the class on the post edit screen.
 */
function education_lms_metabox_init() {
	new Education_LMS_MetaBox();
}
if ( is_admin() ) {
	add_action( 'load-post.php',     'education_lms_metabox_init' );
    add_action( 'load-post-new.php', 'education_lms_metabox_init' );
    
}
/**
 * The Class.
 */
class Education_LMS_MetaBox {
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
        add_action( 'save_post',      array( $this, 'save'         ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'education_load_admin_scripts' ), 10, 1 );
    }
    
    public function education_load_admin_scripts( $hook ) {
		global $typenow;
		if( 'post' ==  $typenow || 'lp_course' ==  $typenow ) {
			wp_enqueue_media();
			// Registers and enqueues the required javascript.
			wp_enqueue_script( 'meta-box-imag', get_template_directory_uri() . '/assets/js/admin.js', array( 'jquery' ), '', false );
			wp_localize_script( 'meta-box-image', 'meta_image',
				array(
					'title' => __( 'Choose or Upload Image', 'education-lms' ),
					'button' => __( 'Use this image', 'education-lms' ),
				)
			);
			wp_enqueue_script( 'meta-box-image' );
		}
    }
    
	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
		// Limit meta box to certain post types.
		$post_types = array( 'page' );
		if ( in_array( $post_type, $post_types ) ) {
			add_meta_box(
				'education_lms_page_settings',
				__( 'Page Settings', 'education-lms' ),
				array( $this, 'render_meta_box_content' ),
				$post_type,
				'side',
				'high'
			);

        }
        
       
            if ( in_array( $post_type, array('post', 'lp_course') ) ) {
                add_meta_box(
                    'education_lms_post_settings',
                    __( 'Custom Header', 'education-lms' ),
                    array( $this, 'render_meta_box_post_course' ),
                    $post_type,
                    'side',
                    'high'
                );
    
            }
        
        
        
	}
	public function save( $post_id ) {
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */
		// Check if our nonce is set.
		if ( ! isset( $_POST['education_lms_page_settings_nonce'] ) ) {
			return $post_id;
		}
		$nonce = sanitize_text_field( $_POST['education_lms_page_settings_nonce'] );
		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'education_lms_page_settings' ) ) {
			return $post_id;
		}
		/*
		 * If this is an autosave, our form has not been submitted,
		 * so we don't want to do anything.
		 */
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
		// Check the user's permissions.
		if ( 'page' == get_post_type( $post_id ) ) {
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return $post_id;
			}
		} else {
			if ( ! current_user_can( 'edit_post', $post_id ) ) {
				return $post_id;
			}
		}
		$settings = isset( $_POST['education_lms_page_settings'] ) ? wp_unslash( $_POST['education_lms_page_settings'] ) : array();
		$settings = wp_parse_args( $settings, array(
			'hide_breadcrumb' => '',
			'hide_title_bar' => '',
			'page_display_cover' => '',
			'disable_header_transparent' => ''
		) );
		foreach( $settings as $key => $value ) {
			// Update the meta field.
			update_post_meta( $post_id, '_'.$key, sanitize_text_field( $value ) );
        }
        
        if ( isset( $_POST['custom_header_image'] ) ) {
            update_post_meta( $post_id, 'custom_header_image', sanitize_text_field( $_POST['custom_header_image'] ) );
        }

	}
	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'education_lms_page_settings', 'education_lms_page_settings_nonce' );
		$values = array(
			'hide_breadcrumb' => '',
			'hide_title_bar' => '',
			'page_display_cover' => '',
            'disable_header_transparent' => ''
		);
		foreach( $values as $key => $value ) {
			$values[ $key ] = get_post_meta( $post->ID, '_'.$key, true );
		}

		?>

		<p>
			<label>
				<input type="checkbox" name="education_lms_page_settings[hide_title_bar]" <?php checked( $values['hide_title_bar'], 1 ); ?> value="1"> <?php _e( 'Hide Titlebar.', 'education-lms' ); ?>
			</label>
		</p>

		<p>
			<label>
				<input type="checkbox" name="education_lms_page_settings[hide_breadcrumb]" <?php checked( $values['hide_breadcrumb'], 1 ); ?> value="1"> <?php _e( 'Hide Breadcrumb.', 'education-lms' ); ?>
			</label>
		</p>

		<p>
			<label>
				<input type="checkbox" name="education_lms_page_settings[page_display_cover]" <?php checked( $values['page_display_cover'], 1 ); ?> value="1"> <?php _e( 'Display featured image as header cover.', 'education-lms' ); ?>
			</label>
		</p>

        <p>
            <label>
                <input type="checkbox" name="education_lms_page_settings[disable_header_transparent]" <?php checked( $values['disable_header_transparent'], 1 ); ?> value="1"> <?php _e( 'Disable Header Transparent.', 'education-lms' ); ?>
            </label>
        </p>


		<?php
    }
    
    public function render_meta_box_post_course( $post ) {

        wp_nonce_field( 'education_lms_page_settings', 'education_lms_page_settings_nonce' );

        $custom_image = get_post_meta( $post->ID, 'custom_header_image', true );

        $image = ' button">Upload image';
        $image_size = 'full'; // it would be better to use thumbnail size here (150x150 or so)
        $display = 'none'; // display state ot the "Remove image" button
    
        if( $image_attributes = wp_get_attachment_image_src( $custom_image, $image_size ) ) {
    
            $image = '"><img src="' . $image_attributes[0] . '" style="max-width:95%;display:block;" />';
            $display = 'inline-block';
    
        } 
    
        echo '
        <p>
            <label>
                <a href="#" class="btn_upload_image' . $image . '</a>
                <input type="hidden" name="custom_header_image" id="custom_header_image" value="' . $custom_image . '" />
                <a href="#" class="btn_remove_image" style="display:inline-block;display:' . $display . '">Remove</a>
            </label>
        </p>';

    }

}