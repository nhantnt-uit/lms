<?php
/**
 * Template for displaying search course form.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/search-form.php
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();
?>

<?php if ( ! ( learn_press_is_courses() || learn_press_is_search() ) ) {
	return;
}

$view_default = education_lms_archive_course_layout();

$view = education_lms_course_view();

if ( is_tax() ) {
	$total = get_queried_object();
	$total = $total->count;
} elseif ( !empty( $_REQUEST['s'] ) ) {
	
	global $wp_query;
	$total = $wp_query->found_posts;
} else {
	$total = wp_count_posts( 'lp_course' );
	$total = $total->publish;
}

if ( $total == 0 ) {
	echo '<p class="message message-error">' . esc_html__( 'There are no available courses!', 'education-lms' ) . '</p>';
	return;
} elseif ( $total == 1 ) {
	$index = esc_html__( 'Showing only one result', 'education-lms' );
} else {

	$courses_per_page = absint( LP()->settings->get( 'archive_course_limit' ) );
	$paged            = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;

	$from = 1 + ( $paged - 1 ) * $courses_per_page;
	$to   = ( $paged * $courses_per_page > $total ) ? $total : $paged * $courses_per_page;

	if ( $from == $to ) {
		$index = sprintf(
			esc_html__( 'Showing last course of %s results', 'education-lms' ),
			$total
		);
	} else {
		$index = sprintf(
			esc_html__( 'Showing %s-%s of %s results', 'education-lms' ),
			$from,
			$to,
			$total
		);
	}
}

?>
<div class="switch-layout-container">
	<div class="switch-layout">
		<a href="?view_type=grid" class="<?php echo ( $view == 'grid' ) ? 'switch-active' : ''; ?>"><i class="fa fa-th-large"></i></a>
		<a href="?view_type=list" class="<?php echo ( $view == 'list' ) ? 'switch-active' : ''; ?>"><i class="fa fa-list-ul"></i></a>
    </div>
    
    <div class="course-results">
			<span><?php echo( $index ); ?></span>
		</div>

	<form method="get" name="search-course" class="learn-press-search-course-form"
	      action="<?php echo learn_press_get_page_link( 'courses' ); ?>">

		<input type="text" name="s" class="search-course-input" value="<?php echo $s; ?>"
		       placeholder="<?php _e( 'Search course...', 'education-lms' ); ?>" autocomplete="off"/>
		<input type="hidden" name="ref" value="course"/>

		<button type="submit" class="search-submit" >
			<i class="fa fa-search"></i>
		</button>

	</form>

</div>
