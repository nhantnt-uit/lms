
(function($) {
    "use strict";

    $('.nav-form li.menu-item-has-children > span.arrow').click(function(){
        $(this).next('ul.sub-menu').slideToggle( 500 );
        $(this).toggleClass('active');
        return false;
    });
    if ( Education_LMS.menu_sidebar == 'dropdown' ) {
        $('#mobile-open').click(function (e) {
            e.preventDefault();
            $('.nav-form').toggleClass('open').css('transition', 'none');
            $(this).toggleClass('nav-is-visible');
        });
    } else {
        $('.mobile-menu').click(function (e) {
            e.preventDefault(); // prevent the default action
            e.stopPropagation(); // stop the click from bubbling

            $('.nav-form').toggleClass('open');
            // click body one click for close menu
            $(document).on('click', function closeMenu (e){
                if ( $('.nav-form').has(e.target).length === 0){
                    $('.nav-form').removeClass('open');
                } else {
                    $(document).on('click', closeMenu);
                }
            });

        });
    }



    
    // Sticky header
    if ( Education_LMS.sticky_header == 1 ) {
        var top_bar = $('.site-header');
        top_bar.wrap('<div class="site-header-wrap">');
        var top_bar_wrap = top_bar.parent();
        top_bar_wrap.addClass('no-fixed');
        top_bar_wrap.css({
            height: top_bar.outerHeight() + 'px',
            display: 'block'
        });

        $( window ).on('resize', function () {
            top_bar_wrap.removeAttr('style');
            top_bar_wrap.css({
                height: top_bar.outerHeight() + 'px',
                display: 'block'
            });
        });

        $( window ).on("scroll", function () {
            var fromTop = $(document).scrollTop();
            var _top = 0;
            var is_fixed_admin = false;
            if ( $('#wpadminbar').length > 0 ) {
                _top = $('#wpadminbar').outerHeight();
                if ( 'fixed' == $('#wpadminbar').css('position') ) {
                    is_fixed_admin = true;
                }
            }
            if ( fromTop > _top ) {
                top_bar_wrap.removeClass('no-fixed');
                top_bar_wrap.addClass('fixed');
                if (is_fixed_admin) {
                    top_bar.css('top', _top + 'px');
                } else {
                    top_bar.css('top', '0px');
                }
            } else {
                top_bar_wrap.addClass('no-fixed');
                top_bar_wrap.removeClass('fixed');
                top_bar.css('top', '0');
            }
        });
    }

    // back to top button
    if ( $('#to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#to-top').addClass('show');
                } else {
                    $('#to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    /*
    * Handle filter form click ajax
    * */
    $('.courses-search-widget form').on('submit', function(e) {
        var input = $(this).find('.search-course-input');

        if ( input.val() === '' ) {
            input.focus();
            return false;
        } 

        return true;
    });


    


})(jQuery);
