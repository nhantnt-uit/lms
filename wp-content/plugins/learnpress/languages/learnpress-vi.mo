��    �      T      �
      �
  
   �
     �
     �
     �
     �
     �
     �
     �
  5   �
          '  #   7     [     b  
   q     |     �  	   �     �     �     �     �     �     �     �     �     �       
   "     -     A     J     O     e  '   n     �     �     �  .   �     �     �     �     �     �  6   �  M   4     �     �  
   �     �     �     �     �     �     �                    -     5     D      Q     r     w  
   �     �     �     �     �     �     �     �     �          2     @     N     U     ^     s     x     �     �     �     �     �     �     �          "     (     /  	   7  	   A     K     Y  	   f     p     u     }     �     �  2   �     �     �     �                     .     :     G     N     W     i  	   }  c   �  b   �  -   N  J   |  
   �     �     �     �     �  
   �     �             &     �   5     �     �     �     �     �     �          $     C     Q     Z     l     r     z     �  $   �  3   �  @   �     >     K     ]     y     �     �     �  
   �     �     �          +     ?     Q     g     o  
   �  �  �        
   /     :     F     _     q     }  
   �  D   �  
   �     �  7   �  
   3     >     V     c     p     }  
   �     �     �     �     �     �     �     �          8     Q     ^     ~     �     �     �  <   �                 4   ,     a     m     r     ~  
   �  H   �  y   �     \     r     �     �     �     �     �     �      �  
              .     B     Q     l  &   z     �     �  	   �     �     �            
   4     ?  
   Q     \     x     �     �     �     �     �  
   �     �                 -     B  0   R     �     �     �     �     �     �     �     �     �     �  
   
   
      
       
   +   
   6      A   W   T   	   �      �      �      �      �      �      �      �      
!     !  (   !  $   F!     k!  �   �!  �   "  >   �"  v   �"     d#     p#     ~#     �#     �#     �#     �#     �#     �#  :   �#  �   �#     �$     �$  
   �$     �$     �$     �$     �$     �$     %      %     2%     R%     g%  '   n%     �%     �%  D   �%  7   &     P&      k&     �&     �&     �&     �&     �&     �&     �&     �&     '     '     '  
   %'  
   0'     ;'     ='   %d student Add Add New Add New Course Category Add New Course Tag Add as New... Add-ons All Allow re-viewing questions after completing the quiz. Archive Archive History Archive quiz results for each time. Author Back to Course Categories Category Complete Completed Content Continue Course Course Categories Course Tags Course results Courses Create a new course Create a new question Create new account Curriculum Curriculum is empty Database Date Duplicate this course Duration Duration of the quiz. Set 0 to disable. Edit Enroll Enrolled Fill out the form and send us your requesting. Free From General Get more (%d) Go How many points minus for each wrong question in quiz. How many times the user can re-take this quiz. Set to 0 to disable re-taking. Installed (%d) Installed add-ons Instructor Instructors Items completed Last 12 Months Last 30 Days Last 7 Days Lesson content is empty. Lessons Login Lost your password? Message Minus For Skip Minus Points Minus points for skip questions. Name New Quiz No content No item found No item found. Order Order status Order statusCancelled Order statusCompleted Order statusFailed Order statusPending Payment Order statusProcessing Order status: Order summary Orders Overview Pagination Questions Paid Passing Grade Passing Grade (<span>%</span>) Password Pending Phone Please %s to send your request! Post Type General NameCourses Post Type Singular NameCourse Preview Price Public Publish Published Purchased Question Bank Question Tag Questions Quiz Quizzes Re-take Register Remember me Requires user reached this point to pass the quiz. Review Questions Search Course Tags Search Courses Search Lessons Search Orders Search course... Search item Search items Select Settings Show Check Answer Show Correct Answer Show Hint Show button to check answer while doing quiz ( 0 = Disabled, -1 = Unlimited, N = Number of check ). Show button to hint answer while doing quiz ( 0 = Disabled, -1 = Unlimited, N = Number of check ). Show correct answer when reviewing questions. Show list of questions while doing quiz as ordered numbers (1, 2, 3, etc). Statistics Status Student Students Submit Submitting Tag Tags Template Thank you! Your message has been sent. The password should be at least twelve characters long. To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ) Themes (%d) To Tools Total Type Type here to search item Type here to search question Type here to search the course Unassigned %s Username Username or email Users Week(s) You are a teacher now. You are a teacher! You have %s remaining for the course Your are logged in as %s. <a href="%s">Log out</a>? Your have already sent the request. Please wait for approvement. Your message Your phone number course-item-navigationNext course-item-navigationPrev courses login-headingLogin page-navigationNext published. quiz-question-navigationNext quiz-question-navigationPrev quiz-question-navigationSkip quiz-resultCorrect quiz-resultPoint quiz-resultQuestions quizzes register-headingRegister week weeks Project-Id-Version: Package Name
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-12-09 10:12+0000
PO-Revision-Date: 2021-02-21 20:10+0000
Last-Translator: 
Language-Team: Tiếng Việt
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.0; wp-5.5.3 %d học viên Thêm vào Thêm mới Thêm mới thể loại Thêm mới thẻ Thêm mới Tiện ích Tất cả Cho phép xem lại câu hỏi sau khi hoàn thành bài kiểm tra. Lưu trữ Lưu trữ lịch sử Lưu trữ kết quả bài kiểm tra cho mỗi lần. Tác giả Trở lại khóa học Thể loại Thể loại Hoàn thành Hoàn thành Nội dung Tiếp tục Khóa học Thể loại khóa học Thẻ khóa học Kết quả khóa học Khóa học Tạo một khóa học mới Tạo một câu hỏi mới Tạo tài khoản mới Giáo trình Giáo trình đang cập nhật Cơ sở dữ liệu  Ngày tháng Nhân bản khóa học này Thời hạn Thời lượng của bài kiểm tra. Chọn 0 để tắt. Cập nhật Ghi danh Đã ghi danh Điền thông tin và gửi yêu cầu của bạn. Miễn phí Từ Tổng quan Cài thêm (%d) Áp dụng Bao nhiêu điểm trừ cho mỗi câu hỏi sai trong bài kiểm tra. Người dùng có thể làm lại bài kiểm tra này bao nhiêu lần. Chọn 0 để tắt tính năng chụp lại. Đã cài đặt (%d) Tiện ích đã cài đặt Giảng viên Giảng viên Các mục đã hoàn thành Một năm qua Một tháng qua 7 ngày qua Nội dung bài học chưa có. Bài học Đăng nhập Quên mật khẩu? Thông điệp Điểm trừ khi bỏ qua Điểm trừ Điểm trừ cho câu hỏi bỏ qua. Tên tài khoản Thêm Mới Chưa có Không tìm thấy mục nào Không tìm thấy mục nào. Đơn đặt hàng Tình trạng đặt hàng Đã hủy Đã hoàn thành Bị lỗi Trong khi chờ thanh toán Đang xử lý Tình trạng đặt hàng Thống kê đặt hàng Đơn đặt hàng Tổng quan Phân trang Trả phí Điểm đạt Điểm đạt (<span>%</span>) Mật khẩu Đang chờ xử lý Điện thoại Vui lòng %s để gửi yêu cầu của bạn! Khóa học Khóa học Xem trước Giá Xuất bản Xuất bản Đã xuất bản Đã mua Thư viện câu hỏi Thẻ câu hỏi Câu hỏi Câu đố Câu đố Làm lại Đăng ký Nhớ tài khoản Yêu cầu người dùng đạt đến điểm này để vượt qua bài kiểm tra. Xem lại Tìm kiếm Tìm kiếm Tìm kiếm Tìm kiếm Tìm kiếm Tìm kiếm Tìm kiếm Chọn Cài đặt Hiển thị kiểm tra câu trả lời Hiển thỉ câu trả lời đúng Hiển thị gợi ý Hiển thị nút để kiểm tra câu trả lời trong khi làm bài kiểm tra (0 = Tắt, -1 = Không giới hạn, N = Số lần kiểm tra). Hiển thị nút để gợi ý câu trả lời trong khi làm bài kiểm tra (0 = Đã tắt, -1 = Không giới hạn, N = Số lần kiểm tra). Hiển thị câu trả lời đúng khi xem lại câu hỏi. Hiển thị danh sách các câu hỏi trong khi làm bài kiểm tra dưới dạng số thứ tự (1, 2, 3, v.v.). Thống kê Trạng thái Học viên Học viên Gửi Đang gửi đi Thẻ Thẻ Bản mẫu Cảm ơn! Thông điệp của bạn đã được gửi. Mật khẩu phải dài tối thiểu 12 ký tự. Để bảo mật tốt hơn, sử dụng chữ hoa và chữ thường, số và các ký hiệu như ! " ? $ % ^ & ) Chủ đề (%d) Đến Công cụ Tổng Loại Tìm kiếm ... Tìm kiếm ... Tìm kiếm ... Chưa sử dụng %s Tên tài khoản Tên tài khoản hoặc e-mail Người sử dụng tuần Bây giờ bạn là một giáo viên. Bạn là một giảng viên! Bạn còn %s cho khóa học Bạn đăng nhập tài khoản %s. <a href="%s">Đăng xuất</a>? Bạn đã gửi yêu cầu. Vui lòng đợi xử lý! Thông điệp của bạn Số điện thoại của bạn Phần tiếp theo Phần trước khóa học   Trang tiếp theo đã xuất bản. Câu hỏi tiếp theo Câu hỏi trước Bỏ qua Đúng Điểm Câu hỏi câu đố   tuần  