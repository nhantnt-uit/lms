��    *      l      �      �     �     �     �     �       �     
   �     �     �     �     �     �  
   �             %        D     J     ^  
   m     x     �  	   �  
   �  	   �     �     �     �          ,     F     K     R     Z  
   j     u     �     �     �     �     �  �  �     [     z  
   �     �     �  �   �     Z     l     �  (   �  
   �  
   �     �     �     	  ;   $	     `	     f	     }	  
   �	  $   �	     �	     �	     �	     �	  "   
     3
  '   H
  (   p
  (   �
  	   �
     �
     �
     �
     �
       	   0  $   :     _     k     x    %1.2f average based on   %d rating %d ratings  Star %d rating %d ratings %s out of 5 stars <strong>%s</strong> addon version %s requires %s version %s or higher is <strong>installed</strong> and <strong>activated</strong>. Add review All post type Amount Display: Average rate: <span>%.1f</span> Cancel Content Course Id: Course comments Course review Display ratings and reviews of course Error Filter by post type Invalid course LearnPress LearnPress - Course Review LearnPress Course Review Load More Loading... New title No review to load Pick up 1 course Please enter the review content Please enter the review title Please select your rating Rate Rating Reviews Select a course Show Rate: Show Review: Star There is no course to select. Title Title: Write a review Project-Id-Version: LearnPress - Course Review
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-05-16 11:29+0700
PO-Revision-Date: 2020-08-03 17:31+0000
Last-Translator: 
Language-Team: Tiếng Việt
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.0; wp-5.4.2  %1.2f trung bình dựa trên  %d đánh giá   Ngôi sao %d đánh giá  %s trên 5 sao <strong>%s</strong> phiên bản addon %s yêu cầu %s phiên bản %s trở lên là <strong>đã cài đặt</strong> và <strong>được kích hoạt</strong>. Thêm đánh giá Tất cả các loại bài Số lượng hiển thị: Tỷ lệ trung bình: <span>%.1f</span> Hủy bỏ Nội dung Mã khóa học: Nhận xét khóa học Đánh giá khóa học Hiển thị xếp hạng và đánh giá của khóa học lỗi Lọc theo loại bài Khóa học không hợp lệ LearnPress LearnPress - Đánh giá khóa học Tìm hiểu khóa học Tải thêm Đang tải... Tiêu đề mới Không có đánh giá để tải Nhận 1 khóa học Vui lòng nhập nội dung đánh giá Vui lòng nhập tiêu đề đánh giá Vui lòng chọn đánh giá của bạn Tỷ lệ Xếp hạng Nhận xét Chọn một khóa học Tỷ lệ hiển thị: Hiển thị đánh giá: Ngôi sao Không có khóa học để chọn. Tiêu đề Tiêu đề: Viết bình luận 